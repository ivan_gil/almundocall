package com.alm.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alm.api.Llamada;
import com.alm.api.ResultadoLlamada;
import com.alm.mvc.control.DispatcherController;

@Controller
@RequestMapping("CallCenterAlmundoWS")
public class WSCallCenterImpl implements WSCallCenter {

	@Autowired
	DispatcherController dispatcher;

	@Override

	@RequestMapping(value = URL_CALL_CENTER, method = RequestMethod.POST, consumes = MediaType.APPLICATION_XML_VALUE, produces = MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody ResultadoLlamada llamar(@RequestBody Llamada llamada) {

		ResultadoLlamada resultado = null;

		while (resultado.getEstado() == 900) {
			resultado = dispatcher.dispatchCall(llamada);
			System.out.println(resultado.getRespuesta() + " --- " + resultado.getResultadosOK());
		}
		return resultado;
	}

}
