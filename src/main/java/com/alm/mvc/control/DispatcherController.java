package com.alm.mvc.control;

import java.util.List;

import com.alm.api.Empleado;
import com.alm.api.Llamada;
import com.alm.api.ResultadoLlamada;

/**
 * @author Ivan Interfaz del controlador de la aplicacion encargada de
 *         direccionar las peticiones - llamadas al PBX
 *
 */
public interface DispatcherController {

	/**
	 * @param llamada
	 *            que esta entrando al callcenter para ser atendida por los
	 *            Empleados
	 * @return Resultado de la llamada, para saber si fue atendida o quedo en
	 *         espera la llamada
	 */
	ResultadoLlamada dispatchCall(Llamada llamada);

	/**
	 * @param empleados
	 *            lista de empleados disponibles para contestar las llamadas en
	 *            sus diferentes cargos
	 */
	void inicializarEmpleados(List<Empleado> empleados);

}
