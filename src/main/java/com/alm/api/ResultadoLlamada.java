package com.alm.api;

import java.util.ArrayList;
import java.util.List;

public class ResultadoLlamada {

	private int estado;
	private String respuesta;
	private List<String> resultadosOK = new ArrayList<>();

	public int getEstado() {
		return estado;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public List<String> getResultadosOK() {
		return resultadosOK;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public void setResultadosOK(List<String> resultadosOK) {
		this.resultadosOK = resultadosOK;
	}

	@Override
	public String toString() {
		return "ResultadoOperacion [estado=" + estado + ", respuesta=" + respuesta + ", resultadosOK=" + resultadosOK
				+ ", getEstado()=" + getEstado() + ", getRespuesta()=" + getRespuesta() + ", getResultadosOK()="
				+ getResultadosOK() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

}
