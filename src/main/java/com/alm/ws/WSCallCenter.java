package com.alm.ws;

import com.alm.api.Llamada;
import com.alm.api.ResultadoLlamada;

/**
 * @author Ivan Servicio Web para recibir llamadas entrantes al Call Center de
 *         Almundo
 *
 */
public interface WSCallCenter {
	String URL_CALL_CENTER = "/llamar";

	/**
	 * @param llamada
	 *            peticion recibida del exterior para ser proesada en el call
	 *            center
	 * @return resultado de llamada , conestada o en lista de espera
	 */
	ResultadoLlamada llamar(Llamada llamada);
}
