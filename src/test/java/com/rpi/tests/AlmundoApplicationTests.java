package com.rpi.tests;

import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alm.api.Director;
import com.alm.api.Empleado;
import com.alm.api.Llamada;
import com.alm.api.Operador;
import com.alm.api.ResultadoLlamada;
import com.alm.api.Supervisor;
import com.alm.mvc.control.DispatcherController;
import com.alm.mvc.model.Pbx;

/**
 * @author Ivan Clase Encargada de ejecutar los test unitarios planeados para
 *         nuestra aplicacion Para las pruebas se envia Hilos simultaneos para
 *         probar la concurrencia
 */
@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(classes = { AppConfig.class })
public class AlmundoApplicationTests {

	@Autowired
	DispatcherController dispatcher;
	@Autowired
	Pbx pbx;

	/**
	 * Test 1: este test nos permite comprobar el comportamiento de nuestro call
	 * center en caso derecibir 9 llamadas simultaneamente , teniendo 10
	 * empleados para poder contestarlas dichas llamadas entrantes. Se revisa el
	 * Ouput de la aplicacion para visualizar el proceso que tuvieron nuestras
	 * llamadas EL test espera aque teminern todos los hilos para dar un
	 * resultado
	 */
	@Test
	public void testMenosDelLimiteLlamadas() {

		System.out.println(
				" _______________________________________________ T E S T  # 1 ________________________________________");

		dispatcher.inicializarEmpleados(armarEmpleados());
		int i;
		for (i = 0; i < 10; i++) {
			Thread peticion = new Thread() {
				public void run() {

					ResultadoLlamada resultado = null;
					Llamada llamada = new Llamada();
					int numero = ((int) (Math.random() * 683999) + 34555);
					llamada.setDescripcion("LLamada ->" + numero);
					llamada.setNumero("3" + numero);
					resultado = dispatcher.dispatchCall(llamada);
					System.out.println(resultado.getRespuesta() + " --- " + resultado.getResultadosOK());
					while (resultado.getEstado() == 900) {
						resultado = dispatcher.dispatchCall(llamada);
						if (resultado.getEstado() == 1)
							System.out.println(resultado.getRespuesta() + " --- " + resultado.getResultadosOK());
					}

				}
			};
			peticion.start();

		}
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		assert (i < 10);
	}

	/**
	 * Test 2: este test nos permite comprobar el comportamiento de nuestro call
	 * center en caso de recibir 20 llamadas simultaneamente , para asi exceder
	 * el maximo de llamadas permitidad y poner las llamadas sobranmtes en
	 * espera hasta quedar plazas libres y asi ser contestadas, teniendo 10
	 * empleados para poder contestarlas dichas llamadas entrantes. Se revisa el
	 * Ouput de la aplicacion para visualizar el proceso que tuvieron nuestras
	 * llamadas EL test espera aque teminen todos los hilos para dar un
	 * resultado
	 */
	@Test
	public void testPasandoLimiteLlamadas() {

		System.out.println(
				" _______________________________________________ T E S T  # 2 ________________________________________");

		dispatcher.inicializarEmpleados(armarEmpleados());
		int i;
		for (i = 0; i < 20; i++) {
			Thread peticion = new Thread() {
				public void run() {

					ResultadoLlamada resultado = null;
					Llamada llamada = new Llamada();
					int numero = ((int) (Math.random() * 683999) + 34555);
					llamada.setDescripcion("LLamada ->" + numero);
					llamada.setNumero("3" + numero);
					resultado = dispatcher.dispatchCall(llamada);
					System.out.println(resultado.getRespuesta() + " --- " + resultado.getResultadosOK());
					while (resultado.getEstado() == 900) {
						resultado = dispatcher.dispatchCall(llamada);
						if (resultado.getEstado() == 1)
							System.out.println(resultado.getRespuesta() + " --- " + resultado.getResultadosOK());
					}

				}
			};
			peticion.start();

		}
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		assert (i < 20);
	}

	/**
	 * @return empleados prefabricados para nuestras pruebas unitarias
	 */
	public List<Empleado> armarEmpleados() {
		Operador operador1 = new Operador("Ivan", 1), operador2 = new Operador("Clara", 2),
				operador3 = new Operador("Martha", 3), operador4 = new Operador("Jesus", 4),
				operador5 = new Operador("Lyda", 5), operador6 = new Operador("Carolina", 6),
				operador7 = new Operador("Cecilia", 7);
		Supervisor supervisor1 = new Supervisor("Armando", 8), supervisor2 = new Supervisor("Luisa", 9);

		Director director = new Director("Jose", 10);

		List<Empleado> empleados = new ArrayList<Empleado>();
		empleados.add(operador1);
		empleados.add(operador2);
		empleados.add(operador3);
		empleados.add(operador4);
		empleados.add(operador5);
		empleados.add(operador6);
		empleados.add(operador7);
		empleados.add(supervisor1);
		empleados.add(supervisor2);
		empleados.add(director);
		return empleados;
	}

}
