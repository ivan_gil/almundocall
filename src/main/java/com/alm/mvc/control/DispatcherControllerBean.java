package com.alm.mvc.control;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alm.api.Empleado;
import com.alm.api.Llamada;
import com.alm.api.ResultadoLlamada;
import com.alm.mvc.model.Pbx;

@Component
public class DispatcherControllerBean implements DispatcherController {

	@Autowired
	Pbx pbx;

	public void setPbx(Pbx pbx) {
		this.pbx = pbx;
	}

	@Override
	public ResultadoLlamada dispatchCall(Llamada llamada) {
		ResultadoLlamada resultado = new ResultadoLlamada();
		if (!pbx.isFull()) {

			pbx.addLlamada(llamada);
			System.out.println("Llamada aceptada [" + llamada.getNumero() + "]");
			resultado.setEstado(1);

			int hora, minutos, segundos, milisegundos;

			resultado.setRespuesta("¬¬¬¬ " + "Operacion exitosa");

			Empleado empleadoAsignado = pbx.asignarLlamada(llamada);
			Calendar calendario = Calendar.getInstance();
			hora = calendario.get(Calendar.HOUR_OF_DAY);
			minutos = calendario.get(Calendar.MINUTE);
			segundos = calendario.get(Calendar.SECOND);
			milisegundos = calendario.get(Calendar.MILLISECOND);
			int duracionVariable = (int) (Math.random() * 6) + 5;
			try {
				Thread.sleep(duracionVariable * 1000);

				resultado.getResultadosOK()
						.add("Llamada [" + llamada.getNumero() + "] atendida por " + empleadoAsignado.getNombre()
								+ " - " + empleadoAsignado.getClass().toString().split("[.]")[3] + "  a las " + hora
								+ ":" + minutos + ":" + segundos + " : " + milisegundos + " -- Duracion (segundos):"
								+ duracionVariable);
				pbx.finalizarLlamada(llamada, empleadoAsignado);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		} else {
			@SuppressWarnings("unused")
			int posicion = pbx.findCola(llamada);

			if (pbx.findCola(llamada) == 0) {
				posicion = pbx.getLlamadasCola().size() + 1;
				pbx.addLlamadaCola(llamada);
				resultado.setEstado(900);
				resultado.setRespuesta("Callcenter ocupado, llamada [" + llamada.getNumero() + "] puesta en espera");
			} else {
				resultado.setEstado(900);
				resultado.setRespuesta("Callcenter ocupado, llamada [" + llamada.getNumero() + "]  puesta en espera");
			}
		}

		return resultado;
	}

	public void inicializarEmpleados(List<Empleado> empleados) {
		pbx.getEmpleados().clear();
		pbx.getEmpleados().addAll(empleados);
	}

}
