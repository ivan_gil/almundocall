package com.rpi.tests;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Ivan Gil AppConfig
 * 
 *         Encargada de levantar el contexto de Spring Boot - Dependencias para
 *         lanzar los Test Unitarios
 *
 */
@Configuration
@ComponentScan({ "com.alm" })
public class AppConfig {
}