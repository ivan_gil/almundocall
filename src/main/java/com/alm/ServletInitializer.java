package com.alm;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

/**
 * @author Ivan ServletInitializer.java Encargada de inicializar el servlet con
 *         las configuraciones basicas como de seguridad y encriptacion
 *
 */
public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		System.setProperty("jasypt.encryptor.password", "rappi");
		return application.sources(CallCenterAlmundoApplication.class);
	}

}
