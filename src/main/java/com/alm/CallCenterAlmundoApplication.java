package com.alm;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.core.env.StandardEnvironment;

import com.ulisesbocchio.jasyptspringboot.environment.EncryptableEnvironment;

/**
 * @author Ivan Gil CallCenterAlmundoApplication
 * 
 *         Encargada de levantar el contexto de Spring Boot - Dependencias
 *
 */
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class CallCenterAlmundoApplication {

	public static void main(String[] args) {

		new SpringApplicationBuilder().environment(new EncryptableEnvironment(new StandardEnvironment()))
				.sources(CallCenterAlmundoApplication.class).run(args);
	}

}
