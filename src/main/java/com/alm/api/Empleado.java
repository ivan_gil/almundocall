package com.alm.api;

/**
 * @author Ivan
 * @category Domain
 */
public class Empleado {

	private String nombre;

	private int id;

	public Empleado(String nombre, int id) {
		super();
		this.nombre = nombre;
		this.id = id;
	}

	/**
	 * getNombre
	 * 
	 * @return nombre del empleado (operador, supervisor, director)
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param cambia
	 *            el nombre de un Empleado
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empleado other = (Empleado) obj;
		if (id != other.id)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

}
