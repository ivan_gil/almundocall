package com.alm.mvc.model;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.alm.api.Constantes;
import com.alm.api.Director;
import com.alm.api.Empleado;
import com.alm.api.Llamada;
import com.alm.api.Operador;
import com.alm.api.Supervisor;

/**
 * @author Ivan Clase encarga de controlar las llamadas entrantes para
 *         asignarlas a un empleado segun las reglas de negocio o para poner la
 *         llamada en lista de espera para ser contestada cuando esten liberados
 *         algun empleado
 *
 */
@Scope(value = "singleton")
@Component
public class Pbx {
	private Queue<Llamada> globalLlamadas = new ConcurrentLinkedQueue<Llamada>();
	private Queue<Llamada> llamadasCola = new ConcurrentLinkedQueue<Llamada>();
	private Queue<Empleado> empleados = new ConcurrentLinkedQueue<Empleado>();
	private Queue<Empleado> empleadosOcupados = new ConcurrentLinkedQueue<Empleado>();
	private boolean full = false;

	/**
	 * @return Lista de llamadas en linea
	 */
	public Queue<Llamada> getGlobalLlamadas() {
		return globalLlamadas;
	}

	/**
	 * @param llamada
	 *            entrante
	 * @return Si fue posible adicionar la llamada segun el limite maximo y
	 *         disponibilidad
	 */
	public boolean addLlamada(Llamada llamada) {
		if (this.globalLlamadas.size() < Constantes.NUMERO_MAX_LLAMADAS - 1) {
			this.globalLlamadas.add(llamada);
			if (llamadasCola.contains(llamada))
				llamadasCola.remove(llamada);
			setFull(false);
			return true;
		} else {
			setFull(true);
			return false;
		}
	}

	/**
	 * @param llamada
	 *            entrante que ya finalizo su proceso con el empleado
	 * @return Si fue posible colgar la llamada
	 */
	public boolean removeLlamada(Llamada llamada) {
		this.globalLlamadas.remove(llamada);
		return true;
	}

	/**
	 * @return Lista de llamadas que se encuentran en cola - espera para ser
	 *         atendidas por un empleado
	 */
	public Queue<Llamada> getLlamadasCola() {
		return llamadasCola;
	}

	/**
	 * @param llamadasCola
	 *            permite modifcar la lista de llamadas en espera - cola
	 */
	public void setLlamadasCola(Queue<Llamada> llamadasCola) {
		this.llamadasCola = llamadasCola;
	}

	/**
	 * @param llamada
	 *            que desea adicionar en espera o cola , mientras se libera un
	 *            empleado
	 */
	public void addLlamadaCola(Llamada llamada) {
		this.llamadasCola.add(llamada);
	}

	/**
	 * @param llamada
	 *            que desea eliminar de la lista de espera porque ya se
	 *            encuebntra disponible para contestar un emkpleado
	 */
	public void removeLlamadaCola(Llamada llamada) {
		this.llamadasCola.remove(llamada);
	}

	/**
	 * @return si se ha superado el maximo de llamadas paralelas parametrizado
	 *         en el call center
	 */
	public boolean isFull() {
		return full;
	}

	/**
	 * @param full
	 *            se alcanzo el limite llamadas permitidas
	 */
	public void setFull(boolean full) {
		this.full = full;
	}

	/**
	 * @return Lista de empleados disponibles para recibir llamadas
	 */
	public Queue<Empleado> getEmpleados() {
		return empleados;
	}

	/**
	 * @param empleados
	 *            Lista de empleadois que se desean adicionar en el call center
	 *            para atender llamadas
	 */
	public void setEmpleados(Queue<Empleado> empleados) {
		this.empleados = empleados;
	}

	/**
	 * @param llamada
	 *            que se desea asignar a un empleado ya que se encuentra
	 *            disponibilidad
	 * @return Empleado asignado para atender la llamada enrante
	 */
	public Empleado asignarLlamada(Llamada llamada) {

		Empleado empleadoAsignado = null;

		while (true) {
			for (Empleado empleado : empleados) {
				if (empleado instanceof Operador && !findOcupados(empleado)) {

					empleadosOcupados.add(empleado);

					empleadoAsignado = empleado;
					return empleadoAsignado;

				}
			}
			for (Empleado empleado : empleados) {
				if (empleado instanceof Supervisor && !findOcupados(empleado)) {
					empleadosOcupados.add(empleado);
					empleadoAsignado = empleado;

					return empleadoAsignado;

				}
			}
			for (Empleado empleado : empleados) {
				if (empleado instanceof Director && !findOcupados(empleado)) {
					empleadosOcupados.add(empleado);
					empleadoAsignado = empleado;

					return empleadoAsignado;

				}
			}
		}

	}

	/**
	 * @param llamada
	 *            que se desea finalizar
	 * @param empleadoAsignado
	 *            que atendio la llamada para finalizar el proceso y poder
	 *            liberarlo para recibir una nueva llamada
	 * @return Si la finalizacion de la llamada fue exitoso
	 */
	public boolean finalizarLlamada(Llamada llamada, Empleado empleadoAsignado) {

		boolean resultado = false;
		globalLlamadas.remove(llamada);

		eliminarOcupados(empleadoAsignado);

		full = false;
		return resultado;
	}

	/**
	 * @param empleado
	 *            que se desea consultar si esta libre
	 * @return si esta libre el empleado consultado
	 */
	public boolean findOcupados(Empleado empleado) {
		for (Empleado empleadoScan : empleadosOcupados) {
			if (empleadoScan.getNombre().equals(empleado.getNombre()))
				return true;
		}
		return false;

	}

	/**
	 * @param llamada
	 *            entrante se encuentra en cola - espera
	 * @return si existe la llamada en cola se envia la posicion de la cola
	 */
	public int findCola(Llamada llamada) {
		int i = 1;
		for (Llamada llamadaScan : llamadasCola) {
			if (llamadaScan.getNumero().equals(llamadaScan.getNumero()))

				return i;
			i++;
		}
		return 0;

	}

	/**
	 * @param empleadoLibre
	 *            liberar o quitar de la lista de empleados ocupados para
	 *            atender una nueva llamada
	 */
	public void eliminarOcupados(Empleado empleadoLibre) {
		if (empleadoLibre != null)
			for (Empleado empleado : empleadosOcupados) {
				if (empleadoLibre.getNombre().equals(empleado.getNombre()))
					empleadosOcupados.remove(empleado);
			}

	}
}
